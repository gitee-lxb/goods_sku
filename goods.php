<?php

if ($_POST) {
    $data           = $_POST;
    $path           = __DIR__ . DIRECTORY_SEPARATOR . 'files' . DIRECTORY_SEPARATOR;
    $goods_path     = $path . 'goods.log';
    $goods_sku_path = $path . 'goods_sku.log';

    if ($data['action_type'] === 'add') {
        $common_data = [
            'goods_id'         => 1,
            'goods_name'       => $data['goods_name'],
            'goods_main_image' => ''
        ];
        $sku_data    = [];

        if ($data['is_multiple_spec']) {
            foreach ($data['goods_sku_data'] as $val) {
                $sku_data[] = array_merge([
                    'sku_id'          => count($sku_data) + 1,
                    'sku_spec_format' => json_encode($val['sku_spec_format'], JSON_UNESCAPED_UNICODE),
                    'spec_name'       => $val['spec_name'],
                    'sku_no'          => $val['sku_no'],
                    'price'           => $val['price'],
                    'org_price'       => $val['org_price'],
                    'stock'           => $val['stock'],
                    'sku_image'       => $val['sku_image'],
                ], $common_data);
            }
        } else {
            $data['goods_spec_format'] = '';
            $sku_data[]                = array_merge([
                'sku_id'          => count($sku_data) + 1,
                'sku_spec_format' => '',
                'spec_name'       => '',
                'sku_no'          => $data['sku_no'],
                'price'           => $data['price'],
                'org_price'       => $data['org_price'],
                'stock'           => $data['stock'],
                'sku_image'       => '',
            ], $common_data);
        }

        $price_data     = array_column($sku_data, 'price');
        $org_price_data = array_column($sku_data, 'org_price');
        $goods_data     = array_merge([
            'is_multiple_spec'  => $data['is_multiple_spec'],
            'goods_spec_format' => empty($data['goods_spec_format']) ? '' : json_encode($data['goods_spec_format'], JSON_UNESCAPED_UNICODE),
            'stock'             => array_sum(array_column($sku_data, 'stock')),
            'min_price'         => min($price_data),
            'max_price'         => max($price_data),
            'min_org_price'     => min($org_price_data),
            'max_org_price'     => max($org_price_data),
        ], $common_data);

        // 暂时用文件保存数据
        file_put_contents($goods_path, json_encode($goods_data, JSON_UNESCAPED_UNICODE));
        file_put_contents($goods_sku_path, json_encode($sku_data, JSON_UNESCAPED_UNICODE));

        echo json_encode([
            'goods_data' => $goods_data,
            'sku_data'   => $sku_data,
        ]);
    } elseif ($data['action_type'] === 'get') {
        echo json_encode([
            'goods_data' => json_decode(file_get_contents($goods_path), true),
            'sku_data'   => json_decode(file_get_contents($goods_sku_path), true),
        ]);
    }
} else {
    require_once './goods.html';
}


